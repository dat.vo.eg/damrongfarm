import { Accordion, Nav, Tab } from "react-bootstrap";
import Slider from "react-slick";
import OrgariumAcc from "../src/components/OrgariumAcc";
import OrgariumCounter from "../src/components/OrgariumCounter";
import Layout from "../src/layouts/Layout";

import { useState } from "react";
import { heroSliderOne, serviceSliderOne } from "../src/sliderProps";
const Index = () => {
  const faqsData = [
    {
      title: "Lời mời hợp tác đối với Nông Dân 4.0",
      type: "Nông Dân",
      content: [
        {
          title:
            "Thuê tối thiểu : từ 01-09 cây sầu Thái từ 5-10 năm tuổi đang cho thu quả vụ 2024",
        },
        {
          title: "Giá thuê: 15 TRIỆU VND/ 1 cây",
        },
        {
          title: "Thời gian thu hoạch : 10 năm",
        },
        {
          title: "Sản lượng thu hoạch 01 cây/ 01 năm : 40GK",
        },
        {
          title: "Tặng : SR Blackthorn trong 5 năm : 5kg/ 1 cây/1 năm",
        },
        {
          title:
            "Tặng: 1 chuyến du lịch 1 người nghỉ dưỡng tham quan farm công ty và tắm suối nước nóng tại năm thứ 1. ( 03 cây trở lên)",
        },
      ],
    },
    {
      title: "Lời mời hợp tác đối với Thương Lái 4.0",
      type: "Thương Lái",
      content: [
        {
          title:
            "Thuê tối thiểu : từ 10-99 cây sầu Thái hoặc Ri6 từ 5-10 năm tuổi đang cho thu quả vụ 2024",
        },
        {
          title: "Giá thuê: 15 TRIỆU VND/ 1 cây",
        },
        {
          title: "Thời gian thu hoạch : 10 năm",
        },
        {
          title: "Sản lượng thu hoạch 01 cây/ 01 năm : 45GK",
        },
        {
          title: "Tặng : SR Blackthorn trong 5 năm : 5kg/ 1 cây/1 năm",
        },
        {
          title:
            "Tặng: 2 chuyến du lịch 1 người nghỉ dưỡng tham quan farm công ty và tắm suối nước nóng tại năm thứ 1 và năm thứ 5",
        },
      ],
    },
    {
      title: "Lời mời hợp tác đối với Đồn Điền Chủ 4.0",
      type: "Đồn Điền Chủ",
      content: [
        {
          title:
            "Thuê tối thiểu : 100 cây gồm 50 cây Ri6 và 50 cây Sầu Thái từ 5-10 năm tuổi đang cho thu quả vụ mùa 2024",
        },
        {
          title: "Giá thuê: 15 TRIỆU VND/ 1 cây",
        },
        {
          title: "Thời gian thu hoạch : 10 năm",
        },
        {
          title: "Sản lượng thu hoạch 01 cây/ 01 năm : 50GK",
        },
        {
          title: "Tặng : SR Blackthorn trong 5 năm : 5kg/ 1 cây/1 năm",
        },
        {
          title:
            "Đồng hưởng 5% tổng sản lượng thu hoạch của khách hàng trong 10 năm",
        },
        {
          title: "Tri ân 1000 m2 đất nông nghiệp có sổ khi hết hợp đồng",
        },
        {
          title:
            "3 chuyến du lịch 2 người nghỉ dưỡng tham quan farm công ty và tắm suối nước nóng tại năm thứ 1 và thứ 5 và 10.",
        },
      ],
    },
  ];
  const [active, setActive] = useState("collapse0");
  return (
    <Layout header={1}>
      <section className="hero-area-one">
        <Slider {...heroSliderOne} className="hero-slider-one">
          <div className="single-slider">
            <div
              className="image-layer bg_cover"
              style={{
                backgroundImage: "url(assets/images/bg/skill-bg-1.jpg)",
              }}
            />
            <div className="container">
              <div className="row justify-content-center">
                <div className="col-lg-10">
                  <div className="hero-content text-center">
                    <h1 data-animation="fadeInUp" data-delay=".5s">
                      Nông Nghiệp &amp; Đầu Tư Nông Nghiệp
                    </h1>
                    <div
                      className="hero-button"
                      data-animation="fadeInDown"
                      data-delay=".6s"
                    ></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="single-slider">
            <div
              className="image-layer bg_cover"
              style={{
                backgroundImage:
                  "url(assets/images/hero/hero_one-slider-22.png)",
              }}
            />
            <div className="container">
              <div className="row justify-content-center">
                <div className="col-lg-10">
                  <div className="hero-content text-center">
                    <h1 data-animation="fadeInUp" data-delay=".5s">
                      Chào mừng bạn đến với Đam Rông Fam
                    </h1>
                    <div
                      className="hero-button"
                      data-animation="fadeInDown"
                      data-delay=".6s"
                    ></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="single-slider">
            <div
              className="image-layer bg_cover"
              style={{
                backgroundImage: "url(assets/images/bg/skill-bg-2.jpg)",
              }}
            />
            <div className="container">
              <div className="row justify-content-center">
                <div className="col-lg-10">
                  <div className="hero-content text-center">
                    <h1 data-animation="fadeInUp" data-delay=".5s">
                      Nông Nghiệp &amp; Hoa quả tươi
                    </h1>
                    <div
                      className="hero-button"
                      data-animation="fadeInDown"
                      data-delay=".6s"
                    ></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Slider>
      </section>
      {/*====== End Hero Section ======*/}
      {/*====== Start Features Section ======*/}
      <section className="features-section">
        <div className="container-1350">
          <div className="features-wrap-one wow fadeInUp">
            <div className="row justify-content-center">
              <div className="col-xl-4 col-md-6 col-sm-12 px-0 px-sm-3">
                <div className="features-item d-flex mb-30">
                  <div className="fill-number">01</div>
                  <div className="icon">
                    <i className="flaticon-tractor" />
                  </div>
                  <div className="text">
                    <h5>Thiết bị nông nghiệp hiện đại</h5>
                    <p>
                      Rút ngắn thời gian công sức và chi phí thấp để sở hữu ngay
                      1 cây sầu riêng đang thu quả của riêng bạn trong vòng 10
                      năm.
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-xl-4 col-md-6 col-sm-12 px-0 px-sm-3">
                <div className="features-item d-flex mb-30">
                  <div className="fill-number">02</div>
                  <div className="icon">
                    <i className="flaticon-agriculture" />
                  </div>
                  <div className="text">
                    <h5>Thu hoạch sầu riêng chín và tươi</h5>
                    <p>
                      Công khai, minh bạch rõ ràng nguồn gốc cây giống, giám sát
                      24/7 công tác chăm sóc tưới tiêu, đảm bảo đúng tiêu chuẩn
                      an toàn.
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-xl-4 col-md-6 col-sm-12 px-0 px-sm-3">
                <div className="features-item d-flex mb-30">
                  <div className="fill-number">01</div>
                  <div className="icon">
                    <i className="flaticon-social-care" />
                  </div>
                  <div className="text">
                    <h5>Rất nhiều nông dân kinh nghiệm &amp; chuyên nghiệp</h5>

                    <p>
                      Giám sát online, Thu hoạch trực tiếp nguồn nông sản sạch
                      từ chính cây giống của bạn.
                    </p>
                    <p>
                      Tăng thu nhập bằng việc mua bán sản phẩm tại Chợ Nông Sản
                      của chúng tôi.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      {/*====== End Features Section ======*/}
      {/*====== Start About Section ======*/}
      <section className="about-section p-r z-1 pt-130 pb-80 pb-xs-40 pt-xs-70">
        <div className="container">
          <div className="row align-items-center">
            <div className="col-xl-5 col-lg-6">
              <div className="about-one_content-box mb-50">
                <div className="section-title section-title-left mb-30 wow fadeInUp">
                  <span className="sub-title">Về chúng tôi</span>
                  <h2>
                    Chúng tôi là công ty nông nghiệp &amp; đầu tư tốt nhất
                  </h2>
                </div>
                <div
                  className="quote-text mb-35 wow fadeInDown"
                  data-wow-delay=".3s"
                >
                  <p>
                    Xây dựng, phát triển và phổ biến sản phẩm của “Nông nghiệp
                    thông minh” và “Canh tác số hóa” đến tay người tiêu dùng.
                  </p>
                </div>
                <Tab.Container defaultActiveKey={"mission"}>
                  <div className="tab-content-box wow fadeInUp">
                    <Nav as={"ul"} className="nav nav-tabs mb-20">
                      <li className="nav-item">
                        <Nav.Link
                          as={"a"}
                          className="nav-link"
                          data-toggle="tab"
                          eventKey="mission"
                          href="#mission"
                        >
                          Tầm nhìn
                        </Nav.Link>
                      </li>
                      <li className="nav-item">
                        <Nav.Link
                          as={"a"}
                          className="nav-link"
                          data-toggle="tab"
                          eventKey="vision"
                          href="#vision"
                        >
                          Xứ mệnh
                        </Nav.Link>
                      </li>
                    </Nav>
                    <Tab.Content className="tab-content">
                      <Tab.Pane className="tab-pane fade" eventKey="mission">
                        <div className="content-box-gap">
                          <li>
                            Trở thành thương hiệu tiên phong về nông nghiệp số
                            hóa 4.0, áp dụng khoa học kĩ thuật công nghệ cao và
                            internet vào quy trình sản xuất, chăm sóc và phân
                            phối
                          </li>
                          <li>
                            Xây dựng, mở rộng diện tích nông trại, phủ xanh đồi
                            trọc bằng đa dạng chủng loại nông sản Việt Nam.
                          </li>
                          <li>
                            Cung ứng các mặt hàng nông sản sạch , giá trị kinh
                            tế cao, an toàn.
                          </li>
                          <li>
                            Tiếp cận thị trường quốc tế, mang nông sản Việt xuất
                            khẩu toàn cầu.
                          </li>
                        </div>
                      </Tab.Pane>
                      <Tab.Pane className="tab-pane fade" eventKey="vision">
                        <div className="content-box-gap">
                          <li>
                            Xây dựng, phát triển và phổ biến sản phẩm của “Nông
                            nghiệp thông minh” và “Canh tác số hóa” đến tay
                            người tiêu dùng.
                          </li>
                          <li>
                            Rút ngắn quá trình “Từ cây con đến thu hoạch” và xóa
                            bỏ ranh giới địa lý để đem “Nông trại về phố”, tạo
                            ra thế hệ nông dân 4.0, giám sát trồng trọt online ,
                            nhận sản phẩm tận nhà thông qua Internet.
                          </li>
                          <li>
                            Cung cấp nông sản sạch, rõ ràng nguồn gốc, xuất xứ
                            và minh bạch quy trình chăm sóc cây trồng.
                          </li>
                          <li>
                            Cung cấp giải pháp tối ưu, mang lại lợi nhuận bền
                            vững lâu dài cho đối tác.
                          </li>
                          <li>
                            Tạo cơ hội việc làm ổn định, nâng cao đời sống vật
                            chất và tinh thần cho người nông dân và đồng bào
                            thiểu số tại địa phương.
                          </li>
                        </div>
                      </Tab.Pane>
                    </Tab.Content>
                  </div>
                </Tab.Container>
              </div>
            </div>
            <div className="col-xl-7 col-lg-6">
              <div className="about-one_image-box p-r mb-50 pl-100">
                <div className="about-img_one wow fadeInLeft">
                  <img src="assets/images/about/6.jpg" alt="About Image" />
                </div>
                <div className="about-img_two wow fadeInRight">
                  <img src="assets/images/about/7.jpg" alt="About Image" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      {/*====== End About Section ======*/}
      {/*====== Start Service Section ======*/}
      <section className="service-section light-gray-bg pt-130 pb-130 pb-xs-40 pt-xs-70">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-xl-6 col-lg-10">
              <div className="section-title text-center mb-50">
                <span className="sub-title">Khu nông trại</span>
                <h2>Khu nông trại theo từng mốc thời gian của chúng tôi</h2>
              </div>
            </div>
          </div>
          <Slider {...serviceSliderOne} className="service-slider-one">
            <div className="single-service-item-two text-center wow fadeInUp">
              <div className="img-holder">
                <img src="assets/images/service/a.jpeg" alt="" />
              </div>
              <div className="text">
                <h3 className="title">KHU NÔNG TRẠI A</h3>
                <p>(03 HECTA)</p>
                <p>150 CÂY SẦU RIÊNG RI6 + THÁI DONA</p>
                <p className="main-btn btn-yellow">2012</p>
              </div>
            </div>
            <div className="single-service-item-two text-center wow fadeInDown">
              <div className="img-holder">
                <img src="assets/images/service/b.png" alt="" />
              </div>
              <div className="text">
                <h3 className="title">KHU NÔNG TRẠI B</h3>
                <p>(1.2 HECTA)</p>
                <p>300 CÂY SẦU RIÊNG RI6 + THÁI DONA</p>
                <p className="main-btn btn-yellow">2013</p>
              </div>
            </div>
            <div className="single-service-item-two text-center wow fadeInUp">
              <div className="img-holder">
                <img src="assets/images/service/c.jpeg" alt="" />
              </div>
              <div className="text">
                <h3 className="title">KHU NÔNG TRẠI C</h3>
                <p>(25 HECTA)</p>

                <p> 1.500 CÂY SẦU THÁI, 10.000 CÂY HẠT DỖI </p>
                <p className="main-btn btn-yellow">2019</p>
              </div>
            </div>
            <div className="single-service-item-two text-center wow fadeInDown">
              <div className="img-holder">
                <img src="assets/images/service/d.jpeg" alt="" />
              </div>
              <div className="text">
                <h3 className="title">KHU NÔNG TRẠI D</h3>
                <p>(1.5 HECTA)</p>
                <p>100 CÂY SẦU RIÊNG RI6 + THÁI DONA</p>
                <p className="main-btn btn-yellow">2020</p>
              </div>
            </div>
            <div className="single-service-item-two text-center wow fadeInUp">
              <div className="img-holder">
                <img src="assets/images/service/e.png" alt="" />
              </div>
              <div className="text">
                <h3 className="title">KHU NÔNG TRẠI E</h3>
                <p>(06 HECTA)</p>
                <p>1.000 GỐC MĂNG LỤC TRÚC</p>
                <p className="main-btn btn-yellow">2022</p>
              </div>
            </div>
            <div className="single-service-item-two text-center wow fadeInDown">
              <div className="img-holder">
                <img src="assets/images/service/f.png" alt="" />
              </div>
              <div className="text">
                <h3 className="title">KHU NÔNG TRƯỜNG SINH THÁI</h3>
                <p>(100 HECTA)</p>
                <p className="main-btn btn-yellow">Đang triển khai...</p>
              </div>
            </div>
          </Slider>
        </div>
      </section>{" "}
      <section className="service-one dark-black-bg pt-130 pb-125 pb-xs-40 pt-xs-70 p-r z-1">
        <div className="shape shape-one">
          <span>
            <img src="assets/images/shape/tree1.png" alt="" />
          </span>
        </div>
        <div className="shape shape-two">
          <span>
            <img src="assets/images/shape/tree2.png" alt="" />
          </span>
        </div>
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-xl-6 col-lg-10">
              <div className="section-title section-title-white text-center mb-60 mb-xs-30 wow fadeInUp">
                <span className="sub-title">Hệ sinh thái VAC</span>
              </div>
            </div>
          </div>
          <div className="row justify-content-center">
            <div className="col-xl-2 col-lg-4 col-md-4 col-sm-12">
              <div className="service-box text-center mb-70 mb-xs-30 wow fadeInUp">
                <div className="icon">
                  <i className="flaticon-wheat-sack" />
                </div>
                <div className="text">
                  <h3 className="title">Khu trang trại gà sản mùa.</h3>
                </div>
              </div>
            </div>
            <div className="col-xl-2 col-lg-4 col-md-4 col-sm-12">
              <div className="service-box text-center mb-70 mb-xs-30 wow fadeInDown">
                <div className="icon">
                  <i className="flaticon-grape" />
                </div>
                <div className="text">
                  <h3 className="title">Khu trang trại lợn giống</h3>
                </div>
              </div>
            </div>
            <div className="col-xl-2 col-lg-4 col-md-4 col-sm-12">
              <div className="service-box text-center mb-70 mb-xs-30 wow fadeInUp">
                <div className="icon">
                  <i className="flaticon-cow" />
                </div>
                <div className="text">
                  <h3 className="title">Khu vực nuôi trùng quế</h3>
                </div>
              </div>
            </div>
            <div className="col-xl-2 col-lg-4 col-md-4 col-sm-12">
              <div className="service-box text-center mb-70 mb-xs-30 wow fadeInDown">
                <div className="icon">
                  <i className="flaticon-fish" />
                </div>
                <div className="text">
                  <h3 className="title">Khu vực ao nuôi thả cá</h3>
                </div>
              </div>
            </div>
            <div className="col-xl-2 col-lg-4 col-md-4 col-sm-12">
              <div className="service-box text-center mb-70 mb-xs-30 wow fadeInUp">
                <div className="icon">
                  <i className="flaticon-healthy-food" />
                </div>
                <div className="text">
                  <h3 className="title">Khu trồng bắp cao</h3>
                </div>
              </div>
            </div>
            <div className="col-xl-2 col-lg-4 col-md-4 col-sm-12">
              <div className="service-box text-center mb-70 mb-xs-30 wow fadeInDown">
                <div className="icon">
                  <i className="flaticon-planet-earth" />
                </div>
                <div className="text">
                  <h3 className="title">Khu vườn ăn trái 4</h3>
                </div>
              </div>
            </div>
            <div className="col-xl-2 col-lg-4 col-md-4 col-sm-12">
              <div className="service-box text-center mb-70 mb-xs-30 wow fadeInDown">
                <div className="icon">
                  <i className="flaticon-planet-earth" />
                </div>
                <div className="text">
                  <h3 className="title">Khu trang trại rau sạch ngắn ngày</h3>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      {/*====== End Counter Section ======*/}
      {/*====== Start Service Section ======*/}
      <section className="popular-service p-r z-1 pt-130 pb-135 pb-xs-40 pt-xs-70">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-xl-8 col-lg-10">
              <div className="section-title section-title-white text-center wow fadeInUp">
                <h2 className="sub-title">Mục tiêu dự án</h2>
              </div>
            </div>
          </div>
          <div className="row align-items-center mt-4">
            <div className="col-lg-6">
              <div className="single-service-item mb-50 wow fadeInUp">
                <div className="icon">
                  <img src="assets/images/icon/icon-1.png" alt="Icon" />
                </div>
                <div className="text">
                  <h3>
                    Phủ xanh khu nông trại C thêm 1.000 cây giống sầu riêng
                    Blackthorn.
                  </h3>
                </div>
              </div>
              <div className="single-service-item mb-50 wow fadeInDown">
                <div className="icon">
                  <img src="assets/images/icon/icon-2.png" alt="Icon" />
                </div>
                <div className="text">
                  <h3>
                    Gây giống và chăm sóc khu vườn trái cây độc lạ 4 mùa tại khu
                    nông trại D và E phục vụ du lịch.
                  </h3>
                </div>
              </div>
            </div>
            <div className="col-lg-6">
              <div className="single-service-item mb-50 card-rtl wow fadeInUp">
                <div className="icon">
                  <img src="assets/images/icon/icon-3.png" alt="Icon" />
                </div>
                <div className="text">
                  <h3>
                    Xây dựng hệ sinh thái Vườn-Ao- Chuồng trong khu nông trại C
                    tạo nguồn thu ngắn ngày.
                  </h3>
                </div>
              </div>
              <div className="single-service-item mb-50 card-rtl wow fadeInDown">
                <div className="icon">
                  <img src="assets/images/icon/icon-4.png" alt="Icon" />
                </div>
                <div className="text">
                  <h3>
                    Hoàn thiện cở sở vật chất cho Khu nông trường sinh thái 100
                    hecta.
                  </h3>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      {/*====== End Testimonial Section ======*/}
      {/*====== Start Why Choose Section ======*/}
      <section className="why-choose-one p-r z-1 ">
        <div className="shape shape-one">
          <span>
            <img src="assets/images/shape/leaf-3.png" alt="" />
          </span>
        </div>
        <div className="shape shape-two">
          <span>
            <img src="assets/images/shape/leaf-2.png" alt="" />
          </span>
        </div>
        <div className="container">
          <div className="row">
            <div className="col-lg-6">
              <div className="choose-one_img-box p-r mb-40 wow fadeInLeft">
                <img
                  src="assets/images/choose/img-1.jpg"
                  className="choose-img_one"
                  alt=""
                />
                <img
                  src="assets/images/choose/img-2.jpg"
                  className="choose-img_two"
                  alt=""
                />
              </div>
            </div>
            <div className="col-lg-6">
              <div className="choose-one_content-box pl-lg-70 mb-40">
                <div className="section-title section-title-left mb-40 wow fadeInDown">
                  <h2>Tiếp thị liên kết</h2>
                </div>
                <div className="choose-item-list wow fadeInUp">
                  <div className="single-choose-item mb-30">
                    <div className="text">
                      <h5>Cấp 1</h5>
                      <p>
                        5% Giá trị tổng số cây SR cho thuê của nhà phân phối cấp
                        1
                      </p>
                    </div>
                  </div>
                  <div className="single-choose-item mb-30">
                    <div className="text">
                      <h5>Cấp 2</h5>
                      <p>
                        3% Giá trị tổng số cây SR cho thuê của nhà phân phối cấp
                        2
                      </p>
                    </div>
                  </div>
                  <div className="single-choose-item mb-30">
                    <div className="text">
                      <h5>Cấp 3</h5>
                      <p>
                        2% Giá trị tổng số cây SR cho thuê của nhà phân phối cấp
                        3
                      </p>
                    </div>
                  </div>
                  <div className="single-choose-item mb-30">
                    <div className="text">
                      <h5>Cấp 4</h5>
                      <p>
                        Bonus 3% doanh số của toàn hệ thống dành cho các Giám
                        Đốc Chi Nhánh
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="faq-section pt-170 pb-110 pb-xs-40 pt-xs-70">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-xl-7 col-lg-10">
              <div className="section-title text-center mb-70 mb-xs-30 wow fadeInUp">
                <span className="sub-title">LỜI MỜI HỢP TÁC</span>

                <h2>LỊCH MỞ BÁN</h2>
                <h4>
                  15 TRIỆU VND/ CÂY &amp; T12/2023 - T7/2024
                  <br />
                  1200 CÂY VỤ MÙA 2024
                </h4>
              </div>
            </div>
          </div>
          <div className="row justify-content-center">
            <div className="col-lg-11">
              <div className="faq-one_content-box wow fadeInDown">
                <Accordion
                  defaultActiveKey="collapse0"
                  className="accordion"
                  id="accordionOne"
                >
                  {faqsData?.map((faq, i) => (
                    <OrgariumAcc
                      title={faq?.title}
                      key={i}
                      event={`collapse${i}`}
                      onClick={() => setActive(`collapse${i}`)}
                      active={active}
                      type={faq?.type}
                      content={faq?.content}
                    />
                  ))}
                </Accordion>
              </div>
            </div>
          </div>
        </div>
      </section>
      {/*====== End Gallery Section ======*/}
      {/*====== Start Counter Section ======*/}
      <section className="fun-fact">
        <div className="big-text mb-105 wow fadeInUp">
          <h2>Số liệu thống kê</h2>
        </div>
        <div className="container">
          <div className="counter-wrap-one wow fadeInDown">
            <div className="counter-inner-box">
              <OrgariumCounter />
            </div>
          </div>
        </div>
      </section>
      {/*====== End Testimonial Section ======*/}
      {/*====== Start Contact Section ======*/}
      <section className=" p-r z-2">
        <div className="container-fluid">
          <div className="row no-gutters">
            <div className="col-lg-6">
              <div className="contact-one_content-box wow fadeInLeft">
                <div className="contact-wrapper">
                  <div className="section-title section-title-left mb-40">
                    <span className="sub-title">Liên hệ</span>
                    <h2>Gửi tin nhắn cho chúng tôi</h2>
                  </div>
                  <div className="contact-form">
                    <form onSubmit={(e) => e.preventDefault()}>
                      <div className="form_group">
                        <input
                          type="text"
                          className="form_control"
                          placeholder="Họ và tên"
                          name="name"
                          required=""
                        />
                      </div>
                      <div className="form_group">
                        <input
                          type="email"
                          className="form_control"
                          placeholder="Địa chỉ email"
                          name="email"
                          required=""
                        />
                      </div>
                      <div className="form_group">
                        <textarea
                          className="form_control"
                          placeholder="Nội dung"
                          name="message"
                          defaultValue={""}
                        />
                      </div>
                      <div className="form_group">
                        <button className="main-btn yellow-bg">Gửi</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-6">
              <div
                className="contact-one_information-box bg_cover wow fadeInRight"
                style={{
                  backgroundImage: "url(assets/images/bg/contact-bg-1.jpg)",
                }}
              >
                <div className="information-box">
                  <h3>Liên hệ</h3>
                  {/* <p>
                    Sit volupta accusantium doloreues laudatiuec totam rem
                    aperiam eaque abillo inventore verit atiset
                  </p> */}
                  <div className="information-item_one d-flex mb-25">
                    <div className="icon">
                      <i className="flaticon-placeholder" />
                    </div>
                    <div className="info">
                      <span className="sub-title mb-1">Địa chỉ</span>
                      <h5>
                        Thị Trấn Bằng Lăng, Huyện Đam Rông, Tỉnh Lâm Đồng.
                      </h5>
                    </div>
                  </div>
                  <div className="information-item_one d-flex mb-25">
                    <div className="icon">
                      <i className="flaticon-email" />
                    </div>
                    <div className="info">
                      <span className="sub-title mb-1">Địa chỉ email</span>
                      <h5>
                        <a href="mailto:hotline@gmail.com">hotline@gmail.com</a>
                      </h5>
                    </div>
                  </div>
                  <div className="information-item_one d-flex mb-25">
                    <div className="icon">
                      <i className="flaticon-phone-call" />
                    </div>
                    <div className="info">
                      <span className="sub-title mb-1">Hotline</span>
                      <h5>
                        <a href="tel:+0123456789">03.58588363</a>
                      </h5>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      {/*====== End Blog Section ======*/}
      {/*====== Start Partner Section ======*/}
      {/* <section className="partners-section yellow-bg pt-50 pb-60 p-r z-1">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-xl-6 col-lg-10">
              <div className="section-title text-center mb-30 wow fadeInUp">
                <h4>Chúng tôi có hơn 12 đối tác toàn cầu</h4>
              </div>
            </div>
          </div>
          <Slider {...logoSlider} className="partner-slider-one wow fadeInDown">
            <div className="partner-item">
              <div className="partner-img">
                <img
                  src="assets/images/partner/img-1.png"
                  alt="partner image"
                />
              </div>
            </div>
            <div className="partner-item">
              <div className="partner-img">
                <img
                  src="assets/images/partner/img-2.png"
                  alt="partner image"
                />
              </div>
            </div>
            <div className="partner-item">
              <div className="partner-img">
                <img
                  src="assets/images/partner/img-3.png"
                  alt="partner image"
                />
              </div>
            </div>
            <div className="partner-item">
              <div className="partner-img">
                <img
                  src="assets/images/partner/img-4.png"
                  alt="partner image"
                />
              </div>
            </div>
            <div className="partner-item">
              <div className="partner-img">
                <img
                  src="assets/images/partner/img-5.png"
                  alt="partner image"
                />
              </div>
            </div>
            <div className="partner-item">
              <div className="partner-img">
                <img
                  src="assets/images/partner/img-6.png"
                  alt="partner image"
                />
              </div>
            </div>
            <div className="partner-item">
              <div className="partner-img">
                <img
                  src="assets/images/partner/img-3.png"
                  alt="partner image"
                />
              </div>
            </div>
          </Slider>
        </div>
      </section> */}
      {/*====== End Partner Section ======*/}
      {/*====== Start Footer ======*/}
      {/*====== End Footer ======*/}
      {/*====== back-to-top ======*/}
    </Layout>
  );
};
export default Index;
