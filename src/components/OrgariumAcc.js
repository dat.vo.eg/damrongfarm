import { Accordion } from "react-bootstrap";
const OrgariumAcc = ({ event, active, onClick, title, type, content = [] }) => {
  return (
    <div className="card mb-20">
      <div className="card-header">
        <Accordion.Toggle
          as="a"
          eventKey={event}
          className="c-pointer"
          aria-expanded={active === event ? "true" : "false"}
          onClick={() => onClick()}
        >
          {title}
        </Accordion.Toggle>
      </div>
      <Accordion.Collapse eventKey={event}>
        <div className="card-body">
          <div className="section-title section-title-left my-3">
            <span className="sub-title">{type} 4.0</span>
          </div>
          <li>{content[0]?.title}</li>
          <li>{content[1]?.title}</li>
          <div className="section-title section-title-left my-3">
            <span className="sub-title">THÀNH QUẢ THU HOẠCH</span>
          </div>
          {content?.slice(2)?.map((item) => (
            <li key={item?.title}>{item?.title}</li>
          ))}
          <h5 className="mt-2">
            Giá thương lái thu mua tại khu nông trại A,B,C vụ 2023: 60.000/ 1 Kg
          </h5>
        </div>
      </Accordion.Collapse>
    </div>
  );
};
export default OrgariumAcc;
