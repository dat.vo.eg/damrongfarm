import Link from "next/link";
import { useEffect, useState } from "react";
import useWindowSize from "../useWindowSize";
import { stickyNav } from "../utils";
import MobileHeader from "./MobileHeader";

const Header = ({ header }) => {
  useEffect(() => {
    stickyNav();
  }, []);
  const [overlayPanel, setOverlayPanel] = useState(false);
  const togglePanel = () => setOverlayPanel(!overlayPanel);

  const { width } = useWindowSize();

  useEffect(() => {
    const headers = document.querySelectorAll(".header-navigation");
    headers.forEach((header) => {
      if (width <= 1199) {
        header.classList.add("breakpoint-on");
      } else {
        header.classList.remove("breakpoint-on");
      }
      // toggle
      const toggleBtn = header.getElementsByClassName("navbar-toggler")[0],
        overlay = header.getElementsByClassName("nav-overlay")[0],
        menu = header.getElementsByClassName("nav-menu")[0];
      toggleBtn.addEventListener("click", () => {
        overlay.classList.add("active");
        menu.classList.add("menu-on");
      });
      overlay.addEventListener("click", () => {
        overlay.classList.remove("active");
        menu.classList.remove("menu-on");
      });
    });
  }, [width]);

  return <Header1 />;
};
export default Header;

const Header1 = () => (
  <header className="header-area">
    <div className="header-middle">
      <div className="container-1350">
        <div className="row align-items-center">
          <div className="col-xl-2 d-xl-block d-lg-none">
            <div className="site-branding d-lg-block d-none">
              <Link href="/" className="brand-logo">
                <img src="assets/images/logo.png" alt="Site Logo" />
              </Link>
            </div>
          </div>
          <div className="col-xl-10 col-lg-12">
            <div className="contact-information">
              <div className="information-item_one d-flex">
                <div className="icon">
                  <i className="flaticon-placeholder" />
                </div>
                <div className="info">
                  <h5 className="mb-1">Địa chỉ</h5>
                  <p>Thị Trấn Bằng Lăng, Huyện Đam Rông, Tỉnh Lâm Đồng.</p>
                </div>
              </div>
              <div className="information-item_one d-flex">
                <div className="icon">
                  <i className="flaticon-email" />
                </div>
                <div className="info">
                  <h5 className="mb-1">Liên hệ</h5>
                  <p>0358588363</p>
                </div>
              </div>
              <div className="button text-md-right text-sm-center">
                <Link href="/">
                  <span className="main-btn btn-yellow">Đầu tư ngay !</span>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div className="header-navigation navigation-one">
      <div className="nav-overlay" />
      <div className="container-1350">
        <div className="primary-menu">
          <div className="site-branding">
            <Link href="/" className="brand-logo">
              <img src="assets/images/logo.png" alt="Site Logo" />
            </Link>
          </div>
          <div className="nav-inner-menu">
            <div className="nav-menu">
              {/*=== Mobile Logo ===*/}
              <div className="mobile-logo mb-30 d-block d-xl-none text-center">
                <Link href="/" className="brand-logo">
                  <img src="assets/images/logo.png" alt="Site Logo" />
                </Link>
              </div>
              {/*=== Main Menu ===*/}
              <Menu />
              <MobileHeader />
            </div>
            {/*=== Nav Right Item ===*/}
            <div className="nav-right-item">
              <div className="navbar-toggler">
                <span />
                <span />
                <span />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>
);

const Menu = () => (
  <nav className="main-menu d-none d-xl-block">
    <ul>
      <li className="menu-item">
        <Link href="/" className="active">
          Trang chủ
        </Link>
      </li>
      <li>
        <Link href="/">Về chúng tôi</Link>
      </li>
      <li className="menu-item">
        <Link href="/">Liên hệ</Link>
      </li>
      <li className="menu-item">
        <a href="#">Bài viết</a>
        {/* has-children */}
        {/* <ul className="sub-menu">
          <li>
            <Link href="blog-standard">Blog Standard</Link>
          </li>
          <li>
            <Link href="blog-details">Blog Details</Link>
          </li>
        </ul> */}
      </li>
    </ul>
  </nav>
);
