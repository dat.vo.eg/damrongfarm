import Link from "next/link";
import { useState } from "react";

const MobileHeader = () => {
  const [activeMenu, setActiveMenu] = useState("");
  const activeMenuSet = (value) =>
      setActiveMenu(activeMenu === value ? "" : value),
    activeLi = (value) =>
      value === activeMenu ? { display: "block" } : { display: "none" };
  return (
    <nav className="main-menu d-block d-xl-none">
      <ul>
        <li className="menu-item has-children">
          <a href="#" className="active">
            Trang chủ
          </a>
        </li>
        <li>
          <Link href="/">Về chúng tôi</Link>
        </li>
        <li className="menu-item">
          <Link href="/">Liên hệ</Link>
        </li>
        <li className="menu-item">
          <a href="#">Bài viết</a>
        </li>
      </ul>
    </nav>
  );
};
export default MobileHeader;
